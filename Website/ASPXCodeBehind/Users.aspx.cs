/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls; //ZD 103405 

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 

namespace ns_MyVRM
{
    public partial class Users : System.Web.UI.Page
    {
        #region Private Variables

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        bool isDeletePresent = false;   //FB 1405
        bool isActionEvents = false; //FB 1405

        private Int32 userlmt = 0;//Organization Module
        private Int32 existinguserlmt = 0;
        StringBuilder inXML = new StringBuilder();//FB 2027
        int enableCloudInstallation = 0;
        string SortingOrder = "0"; //ZD 103405 
        #endregion

        #region Protected Variables

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;
        protected System.Web.UI.WebControls.DropDownList lstBridgeStatus;
        protected System.Web.UI.WebControls.Table tblNoUsers;
        protected System.Web.UI.WebControls.DataGrid dgUsers;
        //protected System.Web.UI.WebControls.TextBox txtBridges; //ZD 100369
        protected System.Web.UI.WebControls.TextBox txtFirstName;
        protected System.Web.UI.WebControls.TextBox txtLastName;
        protected System.Web.UI.WebControls.TextBox txtEmailAddress;
        protected System.Web.UI.WebControls.Table tblAlphabet;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.DropDownList lstSortBy;
        protected System.Web.UI.WebControls.Label lblLicencesRemaining;
        protected System.Web.UI.WebControls.Label lblTotalUsers;
        //protected System.Web.UI.WebControls.Button btnDeleteAllGuest;//ZD 100420
        protected System.Web.UI.HtmlControls.HtmlButton btnDeleteAllGuest;//ZD 100420
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLicencesRemaining;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLegends;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNew;
        //protected System.Web.UI.HtmlControls.HtmlTableRow trNewH; //ZD 100926
        protected System.Web.UI.HtmlControls.HtmlTableRow trSearch;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSearchH;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSearchB;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAction; //FB 1405
        protected int Cloud = 0;//FB 2262
        protected int isLDAP = 0; //ZD 101443

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSortingOrder; //ZD 103405 start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnsort;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnsortBy;
        string Alpha = "ALL";//Edited For FB 1405
        //ZD 103405 End
        protected System.Web.UI.WebControls.Button btnunblockAll;//ZD 103459 
        protected AjaxControlToolkit.ModalPopupExtender modalPopupExtend; //ALLDEV-498
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDeleteUserID; //ALLDEV-498
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdntotalPages; //ALLDEV-498

        #endregion

        #region Constructor
        public Users()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        //ZD 103405 start
        void dguserList_ItemCreated(object sender, DataGridItemEventArgs e)
        {
        }
        //ZD 103405 End 
        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = string.Empty;
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());

                if (Request.QueryString["t"] != null)
                    confChkArg = Request.QueryString["t"].ToString();

                obj.AccessandURLConformityCheck("ManageUser.aspx?t=" + confChkArg.Trim(), Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                if (Session["UserLimit"] != null)
                {
                    if (Session["UserLimit"].ToString() != "")
                        Int32.TryParse(Session["UserLimit"].ToString(), out userlmt);
                }
                dgUsers.ItemCreated += new DataGridItemEventHandler(dguserList_ItemCreated); //ZD 103405 
                //PSU Fix
                if (Application["CosignEnable"] == null)
                    Application["CosignEnable"] = "0";

                //lblHeader.Text = "Manage Guests";   Commented for FB 1405
                errLabel.Text = "";
                if (Request.QueryString["t"] == null)
                    txtType.Text = "1";
                else
                    txtType.Text = Request.QueryString["t"].ToString();
                //Added for FB 1405 - Start
                if (Request.Form["hdnAction"] != null)
                {
                    if (Request.Form["hdnAction"].ToString() == "Y")
                        isActionEvents = true;
                }
                //Added for FB 1405 - End

                //FB 2262,//FB 2599 - Starts //FB 2717 Vidyo Integration Start
                /*if (Session["Cloud"] != null) 
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                if (Cloud == 1)
                {
                    trNewH.Visible = false;
                    trNew.Visible = false;
                }*/
                //FB 2599 - Ends //FB 2717 Vidyo Integration End
                //FB 2262,//FB 2599 - Ends

                //ZD 103405 start
                if (hdnSortingOrder.Value == "1")
                    SortingOrder = "1";
                else
                    SortingOrder = "0";
                //ZD 103405 End
                if (Session["IsLDAP"] != null && Session["IsLDAP"].ToString() != "")
                    int.TryParse(Session["IsLDAP"].ToString(), out isLDAP);

                if (!IsPostBack)
                {
                    switch (txtType.Text)
                    {
                        case "1":
                            lblHeader.Text = obj.GetTranslatedText("Manage Active Users");//FB 1830 - Translation FB 2570
                            if (!Session["admin"].ToString().Equals("2"))
                            {
                                trNew.Visible = false;
                                //trNewH.Visible = false; //ZD 100926
                            }
                            btnunblockAll.Visible = true;//ZD 103459
                            break;
                        case "2":
                            lblHeader.Text = obj.GetTranslatedText("Manage Guests");//FB 1830 - Translation
                            trNew.Visible = false;
                            lstSortBy.Items.Remove(lstSortBy.Items.FindByValue("6"));//ZD 103837
                            lstSortBy.Items.Remove(lstSortBy.Items.FindByValue("5"));//ZD 103837
                            btnunblockAll.Visible = false;//ZD 103459
                            //trNewH.Visible = false; //ZD 100926
                            break;
                        case "3":
                            trNew.Visible = false;
                            //trNewH.Visible = false; //ZD 100926
                            trSearch.Visible = false;
                            trSearchH.Visible = false;
                            trSearchB.Visible = false;
                            lblHeader.Text = obj.GetTranslatedText("Manage Inactive Users");//FB 1830 - Translation
                            lstSortBy.Items.Remove(lstSortBy.Items.FindByValue("6"));//ZD 103837
                            lstSortBy.Items.Remove(lstSortBy.Items.FindByValue("5"));//ZD 103837
                            btnunblockAll.Visible = false;//ZD 103459
                            break;
                        default:
                            errLabel.Text = obj.GetTranslatedText("Invalid User Type");//FB 1830 - Translation
                            break;
                    }
                    //ZD 103837 start
                    if (Session["admin"].ToString().Equals("3"))
                    {
                        lstSortBy.Items.Remove(lstSortBy.Items.FindByValue("6"));
                        btnunblockAll.Visible = false;//ZD 103459
                    }
                    //ZD 103837 End
                    //PSU Fix
                    if ((Application["ssoMode"] != null && Application["ssoMode"].ToString().ToUpper().Equals("YES"))
                    || (Application["CosignEnable"] != null && Application["CosignEnable"].ToString().ToUpper().Equals("1")))
                    {
                        //trNewH.Visible = false; //ZD 100926
                        trNew.Visible = false;
                    }
                    BindDataUser();
                }
                DisplayAlphabets(tblAlphabet);
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                // FB 2664 start
                if (btnDeleteAllGuest.Disabled == false)//ZD 100420
                    btnDeleteAllGuest.Attributes.Add("class", "altLongBlueButtonFormat");
                else
                    btnDeleteAllGuest.Attributes.Add("class", "btndisable");
                //Fb 2664 End
                //ZD 100755 inncrewin
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
            }

        }
        #endregion

        #region BindDataUser

        protected void BindDataUser()
        {
            try
            {
                string rmCasCtrl = "0"; // ZD 100263
                if (Session["roomCascadingControl"] != null)
                    rmCasCtrl = Session["roomCascadingControl"].ToString();

                string pageNo = "1";

                string sortBy = "2";
                if (Request.QueryString["pageNo"] != null)
                    pageNo = Request.QueryString["pageNo"].ToString();
                //ZD 103405  start
                if (Session["DtUsrList"] != null)
                {
                    if (Request.QueryString["sortBy"] != null)
                        sortBy = Request.QueryString["sortBy"].ToString();
                    SortingOrder = hdnSortingOrder.Value;
                }
                else
                {
                    Session["DtUsrSortBy"] = null;
                    Session["DtUsrSortOrder"] = null;
                    Session["DtUsrList"] = null;
                    if (hdnsort.Value == "0")
                    {
                        if (Request.QueryString["sortBy"] != null)
                            sortBy = Request.QueryString["sortBy"].ToString();
                        if (Request.QueryString["sortorder"] != null)
                            SortingOrder = Request.QueryString["sortorder"].ToString();
                    }
                    else
                        sortBy = hdnsortBy.Value;
                }
                lstSortBy.ClearSelection();
                lstSortBy.Items.FindByValue(sortBy).Selected = true;
                hdnsort.Value = "0";
                //ZD 103405 End
                Alpha = "ALL";
                if (Request.QueryString["alpha"] != null)
                {
                    Alpha = Request.QueryString["alpha"].ToString();
                    Alpha = Alpha.Replace("todo", "all").Replace("tout", "all");//ZD 100288 //ZD 101714
                }


                //<login><userID>11</userID><sortBy>2</sortBy><alphabet>A</alphabet><pageNo>1</pageNo></login>
                //FB 2027 Starts
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("  <sortBy>" + sortBy + "</sortBy>");
                inXML.Append("<SortingOrder>" + SortingOrder + "</SortingOrder>"); //ZD 103405
                inXML.Append("  <alphabet>" + Alpha + "</alphabet>");
                inXML.Append("  <pageNo>" + pageNo + "</pageNo>");
                inXML.Append("  <audioaddon>0</audioaddon>"); //FB 2023
                inXML.Append("</login>");
                //Response.Write("<br>" + obj.Transfer(inXML));
                String cmd = "GetManageUser";
                String outXML = "";
                if (txtType.Text.Equals("2"))
                    cmd = "GetManageGuest";
                if (txtType.Text.Equals("3"))
                    cmd = "GetUsers";
                outXML = obj.CallMyVRMServer(cmd, inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 Ends
                //String outXML = "<bridgeInfo><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><bridges><bridge><ID>1</ID><name>Test Bridge</name><interfaceType>3</interfaceType><administrator>VRM Administrator</administrator><exist>1</exist><status>1</status><order></order></bridge></bridges><totalNumber>1</totalNumber><licensesRemain>9</licensesRemain></bridgeInfo>";
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = null;
                    int totalPages = 1;
                    switch (txtType.Text)
                    {
                        case "1":
                            btnDeleteAllGuest.Visible = false;
                            lblTotalUsers.Text = xmldoc.SelectSingleNode("//users/totalNumber").InnerText;
                            Int32.TryParse(xmldoc.SelectSingleNode("//users/totalNumber").InnerText, out existinguserlmt);
                            //lblLicencesRemaining.Text = (userlmt - existinguserlmt).ToString();//xmldoc.SelectSingleNode("//users/licensesRemain").InnerText;
                            //ZD 101443 Starts
                            if (isLDAP == 1)
                                lblLicencesRemaining.Text = obj.GetTranslatedText("N/A"); //FB 2027
                            else
                                lblLicencesRemaining.Text = xmldoc.SelectSingleNode("//users/licensesRemain").InnerText; //FB 2027
                            //ZD 101443 End

                            nodes = xmldoc.SelectNodes("//users/user");
                            totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//users/totalPages").InnerText);
                            break;
                        case "2":
                            tdLicencesRemaining.Visible = false;
                            tdLegends.Visible = false;
                            if (Session["admin"].ToString().Equals("3")) //FB 2670
                                btnDeleteAllGuest.Visible = false;
                            else
                                btnDeleteAllGuest.Visible = true;

                            lblTotalUsers.Text = xmldoc.SelectSingleNode("//users/totalNumber").InnerText;
                            nodes = xmldoc.SelectNodes("//users/user");
                            totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//users/totalPages").InnerText);
                            break;
                        case "3":
                            tdLicencesRemaining.Visible = false;
                            tdLegends.Visible = false;
                            btnDeleteAllGuest.Visible = false;
                            lblTotalUsers.Text = xmldoc.SelectSingleNode("//userInfo/users/totalNumber").InnerText;
                            nodes = xmldoc.SelectNodes("//userInfo/users/user");
                            totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//userInfo/users/totalPages").InnerText);
                            break;
                    }
                    //ZD 103405  satrt
                    string qString = Request.QueryString.ToString();
                    if (Request.QueryString["sortBy"] == null)
                        qString += "&sortBy=" + lstSortBy.SelectedValue;
                    else
                        qString = qString.Replace("&sortBy=" + Request.QueryString["sortBy"].ToString(), "&sortBy=" + lstSortBy.SelectedValue);

                    if (Request.QueryString["sortorder"] == null)
                        qString += "&sortorder=" + SortingOrder;
                    else
                        qString = qString.Replace("&sortorder=" + Request.QueryString["sortorder"].ToString(), "&sortorder=" + SortingOrder);
                    hdntotalPages.Value = totalPages.ToString();
                    //ZD 103405 End
                    if (totalPages > 1)
                    {
                        //  Request.QueryString.Remove("pageNo");
                        if (Request.QueryString["pageNo"] != null)
                            qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                        if (Request.QueryString["m"] != null)
                            qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                        obj.DisplayPaging(totalPages, Int32.Parse(pageNo), tblPage, "ManageUser.aspx?" + qString);
                    }
                    //Response.Write(nodes.Count);
                    if (nodes.Count > 0)
                    {
                        dgUsers.Visible = true;
                        tblNoUsers.Visible = false;
                        LoadGrid(nodes);
                        ShowHide(); //ZD 103405
                        foreach (DataGridItem dgi in dgUsers.Items)
                        {
                            LinkButton btnTemp = (LinkButton)dgi.FindControl("btnDelete" + rmCasCtrl); // ZD 100263
                            if (dgi.Cells[2].Text.Trim().Equals("1"))
                                btnTemp.Text = obj.GetTranslatedText("Undelete");//FB 1830 - Translation
                            btnTemp = (LinkButton)dgi.FindControl("btnLock" + rmCasCtrl); // ZD 100263
                            if (dgi.Cells[3].Text.Trim().Equals("1"))
                                btnTemp.Text = obj.GetTranslatedText("Unblock");//FB 1830 - Translation //ZD 103459
                        }
                    }
                    else
                    {
                        dgUsers.Visible = false;
                        tblNoUsers.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region LoadGrid From Xml

        protected void LoadGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                //Response.Write(ds.Tables[0].Rows[0]["firstName"]);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (ds.Tables.Count > 1)
                    {
                        foreach (DataColumn dc in ds.Tables[1].Columns)
                            if (!dt.Columns.Contains(dc.ColumnName)) dt.Columns.Add(dc.ColumnName);

                        foreach (DataRow dr in dt.Rows)
                            foreach (DataRow drStatus in ds.Tables[1].Rows)
                                if (dr["user_Id"].ToString().Equals(drStatus["user_Id"].ToString()))
                                {
                                    dr["level"] = drStatus["level"];
                                    dr["deleted"] = drStatus["deleted"];
                                    dr["locked"] = drStatus["locked"];
                                    dr["crossaccess"] = drStatus["crossaccess"];
                                }
                    }
                    else
                    {
                        if (!dt.Columns.Contains("level")) dt.Columns.Add("level");
                        if (!dt.Columns.Contains("deleted")) dt.Columns.Add("deleted");
                        if (!dt.Columns.Contains("locked")) dt.Columns.Add("locked");
                        if (!dt.Columns.Contains("crossaccess")) dt.Columns.Add("crossaccess");
                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["level"] = "0";
                            dr["deleted"] = "0";
                            dr["locked"] = "0";
                            dr["crossaccess"] = "0";
                        }
                    }
                    if (!dt.Columns.Contains("RoleName")) //ZD 103415                  
                        dt.Columns.Add("RoleName");
                    //FB 2346
                    string email;
                    string usermail, tempemail;
                    foreach (DataRow drr in ds.Tables[0].Rows)
                    {
                        if (drr["email"].ToString().Trim() != "") //FB 2023
                        {
                            email = drr["email"].ToString();
                            usermail = email.Substring(email.IndexOf('@'));
                            tempemail = usermail;
                            if (usermail.Contains("_D"))
                                usermail = usermail.Replace("_D", "");
                            email = email.Replace(tempemail, usermail);

                            drr["email"] = email;
                        }
                    }
                }
                //ZD 103405 Starts
                Session["DtUsrList"] = null;
                if (txtFirstName.Text != "" || txtLastName.Text != "" || txtEmailAddress.Text != "" || Alpha != "ALL")
                    Session.Add("DtUsrList", ds);

                dgUsers.DataSource = ds;
                dgUsers.DataBind();

                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtEmailAddress.Text = "";


                //TO sort the users in the session - will not hit DB
                if (Session["DtUsrList"] != null && Session["DtUsrSortBy"] != null)
                {
                    DataSet Userds = new DataSet();
                    DataTable firstTable = new DataTable();
                    DataView dv = null;
                    DataTable Userdt = new DataTable();
                    hdnsortBy.Value = Session["DtUsrSortBy"].ToString();

                    dgUsers.CurrentPageIndex = 0;
                    if (hdnsortBy.Value == "1")
                        SortField = "firstName";
                    else if (hdnsortBy.Value == "2")
                        SortField = "lastName";
                    else if (hdnsortBy.Value == "3")
                        SortField = "login";
                    else if (hdnsortBy.Value == "4")
                        SortField = "email";
                    else if (hdnsortBy.Value == "5")
                        SortField = "RoleName";

                    Userds = (DataSet)Session["DtUsrList"];

                    firstTable = Userds.Tables[0];

                    dv = new DataView(firstTable);

                    Userdt = dv.Table;

                    dv.Sort = SortField;

                    if (Session["DtUsrSortOrder"].ToString() == "1")
                        dv.Sort += " DESC";
                    else
                        dv.Sort += " ASC";

                    dgUsers.DataSource = dv;
                    dgUsers.DataBind();
                    ShowHide();
                }
                //ZD 103405 - End

                // Added for FB 1405 -- Start
                if (isDeletePresent)
                {
                    btnDeleteAllGuest.Disabled = false;//ZD 100420
                    btnDeleteAllGuest.Attributes.Add("Class", "altLongBlueButtonFormat");//FB 2664
                }
                else
                {
                    btnDeleteAllGuest.Disabled = true;//ZD 100420
                    btnDeleteAllGuest.Attributes.Add("Class", "btndisable");//FB 2664
                }

                // Added for FB 1405 -- End
                if (!txtType.Text.Equals("1"))
                    dgUsers.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region DeleteUser

        protected void DeleteUser(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                DeleteUser(e.Item.Cells[0].Text.Trim(), e.Item.Cells[2].Text.Trim());
            }
            catch (Exception ex)
            {
                // errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        private void DeleteUser(string userid, string delStatus)
        {
            try
            {

                //Response.Write("In deleteuser");
                //Response.End();
                String inXML = ""; // <login><userID>11</userID><user><userID>26</userID><action>-1</action></user></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <user>";
                inXML += "      <userID>" + userid + "</userID>";
                //Response.Write(e.Item.Cells[2].Text.Trim());
                if (delStatus.Trim().Equals("1"))
                    inXML += "      <action>3</action>";
                else
                    if (txtType.Text.Equals("1"))
                        inXML += "      <action>-1</action>";
                    else
                        inXML += "  <action>-3</action>";
                inXML += "  </user>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String cmdName = "ChangeUserStatus";
                if (txtType.Text.Equals("2"))
                    cmdName = "ChangeGuestStatus";
                String outXML = obj.CallMyVRMServer(cmdName, inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(cmdName);
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    //ZD 100890 Start
                    if (enableCloudInstallation == 1)
                    {
                        string outRadXml = obj.CallCOM2("DeleteUserOnRAD", outXML, Application["RTC_ConfigPath"].ToString());
                        if (outRadXml.ToLower().IndexOf("error") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outRadXml);
                            errLabel.Visible = true;
                        }
                        else
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                    }
                    else
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                    //ZD 100890 End
                    //BindDataUser();
                    Response.Redirect("ManageUser.aspx?m=1&t=" + txtType.Text); //ALLDEV-498
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                // errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #region EditUser
        protected void EditUser(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("UserToEdit", e.Item.Cells[0].Text);
                Response.Redirect("ManageUserProfile.aspx?t=1");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region LockUser
        protected void LockUser(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                if (Session["ConfAdministrator"].ToString().Trim() == e.Item.Cells[0].Text && e.Item.Cells[3].Text.Equals("0"))
                {
                    errLabel.Text = obj.GetTranslatedText("Conference administrator cannot be blocked.");
                    errLabel.Visible = true;

                    DisplayPaging();
                    return;
                }

                String inXML = ""; // <login><userID>11</userID><user><userID>26</userID><action>-1</action></user></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <user>";
                inXML += "      <userID>" + e.Item.Cells[0].Text + "</userID>";
                if (e.Item.Cells[3].Text.Equals("1"))
                    inXML += "      <action>2</action>";
                else
                    inXML += "      <action>-2</action>";
                inXML += "  </user>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("ChangeUserStatus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                    //BindDataUser(); //ZD 103405
                    Response.Redirect("ManageUser.aspx?t=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                string rmCasCtrl = "0"; // ZD 100263
                if (Session["roomCascadingControl"] != null)
                    rmCasCtrl = Session["roomCascadingControl"].ToString();

                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView; //FB 2670
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete" + rmCasCtrl); // ZD 100263
                    //if (Cloud == 1) //FB 2262 //FB 2599 //FB 2717 Vidyo Integration
                    //    ((LinkButton)e.Item.FindControl("btnDelete")).Enabled = false;

                    String txtText = "";
                    //Response.Write("<br>'" + e.Item.Cells[2].Text.Trim() + "'");
                    if (e.Item.Cells[2].Text.Trim().Equals("1"))
                    // Added for FB 1405 -- Start
                    {
                        btnTemp.Text = obj.GetTranslatedText("Undelete");//FB 1830 - Translation
                        txtText = "UNDELETE";
                    }
                    else
                    {
                        btnTemp.Text = obj.GetTranslatedText("Delete");//FB 1830 - Translation
                        txtText = "DELETE";
                        isDeletePresent = true;
                    }

                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "2")
                    {
                        btnTemp.Visible = false;
                    }
                    // Added for FB 1405 -- End

                    // fogbugz case 363 starts here
                    if (btnTemp.Enabled)
                    {
                        Label lblfirstName = (Label)e.Item.FindControl("lblfirstName");
                        Label lbllastName = (Label)e.Item.FindControl("lbllastName");
                        //ALLDEV-498
                        if (e.Item.Cells[11].Text == "1")
                            btnTemp.Attributes.Add("onclick", "return showModelPopup('" + e.Item.Cells[0].Text.Trim() + "_" + e.Item.Cells[2].Text.Trim() + "_" + lblfirstName.Text + " " + lbllastName.Text + "');");
                        else if (e.Item.Cells[11].Text == "2")
                            btnTemp.Attributes.Add("onclick", "return showConfAdminError();");
                        else
                            btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to " + txtText + " this user account?") + "')"); //FB japnese
                    }
                    txtText = "BLOCK";
                    //ZD 101175 Starts
                    if (e.Item.Cells[3].Text.Trim().Equals("1"))
                    {
                        txtText = "UNBLOCK";
                        btnTemp = (LinkButton)e.Item.FindControl("btnLock" + rmCasCtrl); // ZD 100263
                        btnTemp.Text = obj.GetTranslatedText("Unblock");//ZD 103459 
                    }
                    else
                        btnTemp = (LinkButton)e.Item.FindControl("btnLock" + rmCasCtrl); // ZD 100263
                    //ZD 101175 Ends

                    //if (Cloud == 1) //FB 2262 //FB 2599 //FB 2717 Vidyo Integration
                    //    ((LinkButton)e.Item.FindControl("btnLock")).Enabled = false;
                    if (btnTemp.Enabled)
                        btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to " + txtText + " this user account?") + "')"); //FB japnese
                    btnTemp = (LinkButton)e.Item.FindControl("btnReset" + rmCasCtrl); // ZD 100263
                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "3")
                    {
                        //ZD 100263
                        btnTemp.Visible = false;
                        //btnTemp.Enabled = false;
                    }
                    if (btnTemp.Enabled)
                        btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to ACTIVATE this user account ?") + "')"); //FB japnese
                    btnTemp = (LinkButton)e.Item.FindControl("btnDeleteInactive" + rmCasCtrl); // ZD 100263
                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "3")
                    {
                        //ZD 100263
                        btnTemp.Visible = false;
                        //btnTemp.Enabled = false;
                    }
                    if (btnTemp.Enabled)
                        btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to DELETE this user account. This user will not longer be available to restore ?") + "')"); //FB japnese

                    //if (Cloud == 1) //FB 2262 //FB 2599 //FB 2717 Vidyo Integration
                    //    ((LinkButton)e.Item.FindControl("btnDeleteInactive")).Enabled = false;

                    // fogbugz case 363 ends here

                    /* *** FB 1401 To alert the user on edit of the blocked user - Start *** */
                    btnTemp = (LinkButton)e.Item.FindControl("btnEdit" + rmCasCtrl); // ZD 100263
                    if (btnTemp.Enabled)
                    {
                        if (e.Item.Cells[3].Text.Trim().Equals("1"))
                            btnTemp.Attributes.Add("onclick", "alert('" + obj.GetTranslatedText("You must UNBLOCK this user before editing their profile.") + "');DataLoading(0);return false;"); //ZD 102363
                    }
                    //FB 2670
                    if (Session["admin"].ToString() == "3" && txtType.Text == "1")
                    {
                        if (row["userID"].ToString() != "11" && Session["userID"].ToString() != row["userID"].ToString()
                            && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || row["crossaccess"].ToString() != "1"))
                            btnTemp.Enabled = true;
                        else
                            btnTemp.Enabled = false;
                        btnTemp.Text = obj.GetTranslatedText("View");
                        //ZD 100263
                        btnTemp.Visible = false;
                    }
                    /* *** FB 1401 To alert the user on edit of the blocked user - End *** */
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region CreateNewUser
        protected void CreateNewUser(Object sender, EventArgs e)
        {
            Session.Add("UserToEdit", "new");
            Response.Redirect("ManageUserProfile.aspx?t=1");
        }
        #endregion

        #region SearchUser
        protected void SearchUser(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<user>");
                inXML.Append("<firstName>" + txtFirstName.Text + "</firstName>");
                inXML.Append("<lastName>" + txtLastName.Text + "</lastName>");
                inXML.Append("<email>" + txtEmailAddress.Text + "</email>");
                inXML.Append("<type>" + txtType.Text + "</type>");//FB 2027
                inXML.Append("<sortBy>" + lstSortBy.SelectedValue + "</sortBy>");//ZD 103405 
                inXML.Append("<SortingOrder>" + SortingOrder + "</SortingOrder>"); //ZD 103405 
                inXML.Append("</user>");
                inXML.Append("</login>");
                /*Edited for FB 1405 - Start
                Response.Write(obj.Transfer(outXML));*/
                String outXML = obj.CallMyVRMServer("SearchUserOrGuest", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    dgUsers.Visible = false;
                    tblNoUsers.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                    if (nodes.Count > 0)
                    {
                        dgUsers.Visible = true;
                        tblNoUsers.Visible = false;
                        LoadGrid(nodes);
                        ShowHide();//ZD 103405
                        tblPage.Visible = false;//ZD 103405
                    }
                    else
                    {
                        dgUsers.Visible = false;
                        tblNoUsers.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region ChangeAlphabets
        protected void ChangeAlphabets(Object sender, EventArgs e)
        {
            try
            {

                Session["DtUsrSortBy"] = null;
                Session["DtUsrSortOrder"] = null;
                Session["DtUsrList"] = null;

                string qString = Request.QueryString.ToString();
                if (Request.QueryString["sortBy"] == null)
                    qString += "&sortBy=" + lstSortBy.SelectedValue;
                else
                    qString = qString.Replace("&sortBy=" + Request.QueryString["sortBy"].ToString(), "&sortBy=" + lstSortBy.SelectedValue);
                //ZD 103405 start
                if (Request.QueryString["sortorder"] == null)
                    qString += "&sortorder=" + SortingOrder;
                //ZD 103405 End
                if (Request.QueryString["pageNo"] == null)
                    qString += "&pageNo=1";
                else
                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "&pageNo=1");
                //Response.Write(qString);
                Response.Redirect("ManageUser.aspx?" + qString);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region DisplayAlphabets
        protected void DisplayAlphabets(Table tblAlpha)
        {
            try
            {
                string All = obj.GetTranslatedText("All");
                String[] Alphabets = { "a", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", All };
                TableRow tr = new TableRow();
                TableCell tc;
                tblAlpha.Rows.Clear();
                string alpha = "";
                if (Request.QueryString["alpha"] != null)
                    alpha = Request.QueryString["alpha"].ToString();
                //Added for FB 1405 - Start

                if (isActionEvents)
                    alpha = "";

                //Added for FB 1405 - End
                for (int i = 0; i < Alphabets.Length; i++)
                {
                    tc = new TableCell();
                    if (alpha.Equals(Alphabets[i]) || ((i == Alphabets.Length - 1) && (alpha.ToLower().Equals(Alphabets[i].ToLower()))))
                    {
                        if (i == 0)
                            Alphabets[i] = "0-A";//ZD 102367
                        tc.Text = Alphabets[i];
                    }
                    else
                    {
                        string qString = "";
                        qString += "t=" + txtType.Text;
                        if (i == Alphabets.Length - 1)
                            qString += "&alpha=" + Alphabets[i].ToLower();
                        else
                            qString += "&alpha=" + Alphabets[i];
                        if (Request.QueryString["sortBy"] != null)
                            qString += "&sortBy=" + Request.QueryString["sortBy"];
                        else
                            qString += "&sortBy=" + lstSortBy.SelectedValue;
                        //ZD 103405 satrt
                        if (Request.QueryString["sortorder"] != null)
                            qString += "&sortorder=" + Request.QueryString["sortorder"];
                        else
                            qString += "&sortorder=" + SortingOrder;
                        //ZD 103405 End

                        qString += "&pageNo=1";
                        //Response.Write("<br>" + qString);
                        //Response.End();
                        if (i == 0)
                            Alphabets[i] = "0-A";//ZD 102367
                        tc.Text = "<a href='ManageUser.aspx?" + qString + "'>" + Alphabets[i] + "</a>";
                    }
                    tr.Cells.Add(tc);
                }
                tblAlpha.Rows.Add(tr);
            }
            catch (Exception ex)
            {
                // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region DeleteAllGuest
        protected void DeleteAllGuest(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID></userID></user></login>";//Organization Module Fixes
                //String outXML = obj.CallCOM("DeleteAllGuests", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("DeleteAllGuests", inXML, Application["COM_ConfigPath"].ToString()); //FB 2027
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    BindDataUser();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

        #region RestoreUser
        protected void RestoreUser(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //<login><userID>11</userID><users><user><userID>13</userID><mode>2</mode></user></users></login>
                //FB 2027 - Starts
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<users>");
                inXML.Append("<user>");
                inXML.Append("<userID>" + e.Item.Cells[0].Text + "</userID>");
                inXML.Append("<mode>" + e.CommandArgument.ToString() + "</mode>");
                inXML.Append("</user>");
                inXML.Append("</users>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                //Response.Write(obj.Transfer(inXML));
                //FB 2027
                //String outXML = obj.CallCOM("SetUserStatus", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("SetUserStatus", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write("<br>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                    BindDataUser();

                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion


        //ZD 103405 start
        #region SortGrid
        /// <summary>
        /// SortGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SortGrid(Object sender, CommandEventArgs e)
        {
            string qString = "";
            DataGridItem dgi = (DataGridItem)dgUsers.Controls[0].Controls[0];
            hdnsort.Value = "1";
            hdnsortBy.Value = e.CommandArgument.ToString();

            Session["DtUsrSortBy"] = null;
            Session["DtUsrSortOrder"] = null;
            if (Session["DtUsrList"] != null)
            {
                DataSet ds = new DataSet();
                DataTable firstTable = new DataTable();
                DataView dv = null;
                DataTable dt = new DataTable();
                //lstSortBy.SelectedValue = e.CommandArgument.ToString();

                Session["DtUsrSortBy"] = e.CommandArgument.ToString();
                if (!SortAscending)
                    Session["DtUsrSortOrder"] = "1";
                else
                    Session["DtUsrSortOrder"] = "0";


                dgUsers.CurrentPageIndex = 0;
                if (hdnsortBy.Value == "1")
                    SortField = "firstName";
                else if (hdnsortBy.Value == "2")
                    SortField = "lastName";
                else if (hdnsortBy.Value == "3")
                    SortField = "login";
                else if (hdnsortBy.Value == "4")
                    SortField = "email";
                else if (hdnsortBy.Value == "5")
                    SortField = "RoleName";

                ds = (DataSet)Session["DtUsrList"];

                firstTable = ds.Tables[0];

                dv = new DataView(firstTable);

                dt = dv.Table;

                dv.Sort = SortField;

                if (!SortAscending)
                    dv.Sort += " DESC";
                else
                    dv.Sort += " ASC";

                dgUsers.DataSource = dv;
                dgUsers.DataBind();
                ShowHide();
            }
            else
            {

                qString = Request.QueryString.ToString();
                if (Request.QueryString["sortBy"] == null)
                    qString += "&sortBy=" + hdnsortBy.Value;
                else
                    qString = qString.Replace("&sortBy=" + Request.QueryString["sortBy"].ToString(), "&sortBy=" + hdnsortBy.Value);

                if (Request.QueryString["sortorder"] == null)
                    qString += "&sortorder=" + SortingOrder;
                else
                    qString = qString.Replace("&sortorder=" + Request.QueryString["sortorder"].ToString(), "&sortorder=" + SortingOrder);

                Response.Redirect("ManageUser.aspx?" + qString);
            }

        }
        #endregion

        #region ShowHide
        protected void ShowHide()
        {
            try
            {
                string SortBYOrder = "0", SortBYWhat = "2";

                if (Session["DtUsrSortOrder"] != null)
                    SortBYOrder = Session["DtUsrSortOrder"].ToString();
                else
                    SortBYOrder = SortingOrder;

                if (Session["DtUsrSortBy"] != null)
                    SortBYWhat = Session["DtUsrSortBy"].ToString();
                else
                    SortBYWhat = lstSortBy.SelectedValue;

                DataGridItem dgi = (DataGridItem)dgUsers.Controls[0].Controls[0];

                switch (SortBYWhat)
                {
                    case "1":
                        ((HtmlTableCell)dgi.FindControl("tdFirstName")).Attributes.Add("style", "text-decoration:underline");
                        ((HtmlTableCell)dgi.FindControl("tdLastName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdLogin")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdEmail")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdRole")).Attributes.Add("style", "text-decoration:");
                        break;
                    case "2":
                        ((HtmlTableCell)dgi.FindControl("tdLastName")).Attributes.Add("style", "text-decoration:underline");
                        ((HtmlTableCell)dgi.FindControl("tdFirstName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdLogin")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdEmail")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdRole")).Attributes.Add("style", "text-decoration:");
                        break;
                    case "3":
                        ((HtmlTableCell)dgi.FindControl("tdFirstName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdLastName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdLogin")).Attributes.Add("style", "text-decoration:underline");
                        ((HtmlTableCell)dgi.FindControl("tdEmail")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdRole")).Attributes.Add("style", "text-decoration:");
                        break;
                    case "4":
                        ((HtmlTableCell)dgi.FindControl("tdFirstName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdLastName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdEmail")).Attributes.Add("style", "text-decoration:underline");
                        ((HtmlTableCell)dgi.FindControl("tdLogin")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdRole")).Attributes.Add("style", "text-decoration:");
                        break;
                    case "5":
                        ((HtmlTableCell)dgi.FindControl("tdFirstName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdLastName")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdEmail")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdLogin")).Attributes.Add("style", "text-decoration:");
                        ((HtmlTableCell)dgi.FindControl("tdRole")).Attributes.Add("style", "text-decoration:underline");
                        break;
                }
                if (SortBYOrder == "1")
                {
                    ((Label)dgi.FindControl("spnAscendingFirstName")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingLastName")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingLogin")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingEmail")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingRole")).Visible = false;


                    ((Label)dgi.FindControl("spnAscendingFirstNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingLastNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingLoginIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingEmailIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingRoleIE")).Visible = false;


                    ((Label)dgi.FindControl("spnDescndingFirstName")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingLastName")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingLogin")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingEmail")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingRole")).Visible = true;


                    ((Label)dgi.FindControl("spnDescndingFirstNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingLastNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingLoginIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingEmailIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingRoleIE")).Visible = true;


                }
                else
                {
                    ((Label)dgi.FindControl("spnAscendingFirstName")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingLastName")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingLogin")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingEmail")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingRole")).Visible = true;


                    ((Label)dgi.FindControl("spnAscendingFirstNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingLastNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingLoginIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingEmailIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingRoleIE")).Visible = true;


                    ((Label)dgi.FindControl("spnDescndingFirstName")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingLastName")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingLogin")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingEmail")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingRole")).Visible = false;


                    ((Label)dgi.FindControl("spnDescndingFirstNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingLastNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingLoginIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingEmailIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingRoleIE")).Visible = false;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ShowHide:" + ex.Message);
            }
        }

        #endregion
        //ZD 103405 End

        //ZD 103405 - Start
        #region Sort Order

        string SortField
        {
            get
            {
                object o = ViewState["SortField"];
                if (o == null)
                    return String.Empty;
                return (string)o;
            }

            set
            {
                if (value == SortField)
                    SortAscending = !SortAscending;
                ViewState["SortField"] = value;
            }
        }

        bool SortAscending
        {

            get
            {
                object o = ViewState["SortAscending"];
                if (o == null)
                    return true;
                return (bool)o;
            }

            set
            {
                ViewState["SortAscending"] = value;
            }
        }

        #endregion
        //ZD 103405 - End

        //ZD 103459 start

        #region UnBlockAlluser
        protected void UnBlockAlluser(object sender, EventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><user><userID>26</userID><action>-1</action></user></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += " <allusers>1</allusers>";
                inXML += "<UnBlockAlluser>1</UnBlockAlluser>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("SetBulkUserLock", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                    BindDataUser();
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        //ZD 103459 End
        //ALLDEV-498
        #region ViewAssignedUserReport
        protected void ViewAssignedUserReport(object sender, EventArgs e)
        {
            DataSet ds;
            try
            {
                inXML = new StringBuilder();
                inXML.Append("<GetUsageReports>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<DelUserID>" + hdnDeleteUserID.Value.Split('_')[0] + "</DelUserID>");
                inXML.Append("<ReportType>AUR</ReportType>");
                inXML.Append("</GetUsageReports>");

                string outXML = obj.CallMyVRMServer("GetUsageReports", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));
                    string fileName = "View Report (" + hdnDeleteUserID.Value.Split('_')[2] + ")";
                    String destFile = Request.MapPath(".").ToString() + "\\upload" + "\\" + fileName + ".xls";
                    ExportExcel expExcel = new ExportExcel();
                    string[] strSheets = new string[2];
                    strSheets[0] = obj.GetTranslatedText("Conference Details");
                    //strSheets[1] = "Work Order";
                    strSheets[1] = obj.GetTranslatedText("Others");
                    expExcel.GenerateReport(ds, strSheets, "", "", destFile, "users", obj);

                    //To open the Excel files
                    System.IO.FileStream fs = null;
                    fs = System.IO.File.Open(destFile, System.IO.FileMode.Open);
                    byte[] btFile = new byte[fs.Length];
                    fs.Read(btFile, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    Response.Clear();
                    Response.AddHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.BinaryWrite(btFile);
                    Response.End();
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region DeleteUserAssignedDetails
        protected void DeleteUserAssignedDetails(Object sender, EventArgs e)
        {
            DataSet ds;
            try
            {
                if (Session["ConfAdministrator"] == null || (Session["ConfAdministrator"] != null && Session["ConfAdministrator"].ToString().Trim() == ""))
                {
                    errLabel.Text = obj.GetTranslatedText("Please set Conference Administrator before deleting the user.");
                    errLabel.Visible = true;
                    DisplayPaging();
                    return;
                }
                else if (Session["ConfAdministrator"].ToString().Trim() == hdnDeleteUserID.Value.Split('_')[0])
                {
                    errLabel.Text = obj.GetTranslatedText("Conference administrator cannot be deleted.");
                    errLabel.Visible = true;
                    DisplayPaging();
                    return;
                }
                else
                {
                    inXML = new StringBuilder();
                    inXML.Append("<DeleteUserAssignedDetails>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                    inXML.Append("<DelUserID>" + hdnDeleteUserID.Value.Split('_')[0] + "</DelUserID>");
                    inXML.Append("<SelAdminID>" + Session["ConfAdministrator"].ToString() + "</SelAdminID>");
                    inXML.Append("</DeleteUserAssignedDetails>");

                    string outXML = obj.CallMyVRMServer("DeleteUserAssignedDetails", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") < 0)
                    {
                        DeleteUser(hdnDeleteUserID.Value.Split('_')[0], hdnDeleteUserID.Value.Split('_')[1]);

                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }
                    else
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion
        //ALLDEV-498
        private void DisplayPaging()
        {
            if (hdntotalPages.Value != "1")
            {
                string qString = Request.QueryString.ToString();
                if (Request.QueryString["pageNo"] != null)
                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                if (Request.QueryString["m"] != null)
                    qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                obj.DisplayPaging(Int32.Parse(hdntotalPages.Value), 1, tblPage, "ManageUser.aspx?" + qString);
            }
        }
    }
}
